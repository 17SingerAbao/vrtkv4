﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnightController : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed = 4;
    float gravity = 9.8f;
    float rotationSpeed = 300f;
    Vector3 moveDirection = Vector3.zero;
    CharacterController controller;
    Animator animator;
    Vector3 destination;
    public GameObject pointer;


    void Start()
    {
        controller = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
        destination = pointer.GetComponent<RayCastShoot>().hit.point;

    }

    void FixedUpdate()
    {
        destination = pointer.GetComponent<RayCastShoot>().hit.point;
        Vector3 diff = destination - transform.position;
        diff.y = 0f;
        if (diff.magnitude > 0.05f) {
            Move();
        }
    }

    
    void Move()
    {
        Vector3 diff = destination - transform.position;
   
        diff.y = 0f;
        transform.rotation = Quaternion.RotateTowards(transform.rotation, 
                    Quaternion.LookRotation(diff), Time.deltaTime * rotationSpeed);

        if (controller.isGrounded) {
            if (diff.magnitude > 0.05f)
            {
                animator.SetBool("move", true);
                animator.SetInteger("condition", 1);
                
                moveDirection = diff.normalized * speed;

            } else {
                animator.SetBool("move", false);
                animator.SetInteger("condition", 0);
                moveDirection = new Vector3(0, 0, 0);
            }

        }    
        moveDirection.y -= gravity * Time.deltaTime;
        controller.Move(moveDirection * Time.deltaTime);
    }
}

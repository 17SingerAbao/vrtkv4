﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayCastShoot : MonoBehaviour
{
    public float weaponRange = 50f;                                     
    public Transform gunEnd;                                                                                         
    // private WaitForSeconds shotDuration = new WaitForSeconds(0.07f);   
    private LineRenderer laserLine;                                                                            
    public RaycastHit hit;

    public GameObject referencePointer;
    void Start () 
    {
        laserLine = GetComponent<LineRenderer>();
    }


    void FixedUpdate () 
    {
        // StartCoroutine(ShotEffect());
        laserLine.enabled = true;

        Vector3 rayOrigin = gunEnd.position;

        laserLine.SetPosition (0, gunEnd.position);

        if (Physics.Raycast(rayOrigin, transform.forward, out hit, weaponRange))
        {
            laserLine.SetPosition(1, hit.point);
            referencePointer.transform.position = hit.point;
        } else {
            laserLine.SetPosition(1, rayOrigin + transform.forward * weaponRange);
        }
    }

    // private IEnumerator ShotEffect()
    // {
    //     // Play the shooting sound effect

    //     // Turn on our line renderer
    //     laserLine.enabled = true;

    //     //Wait for .07 seconds
    //     yield return shotDuration;

    //     // Deactivate our line renderer after waiting
    //     laserLine.enabled = false;
    // }
}

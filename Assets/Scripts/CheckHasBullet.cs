﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckHasBullet : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject bulletPrefab;
    public float bulletSpeed;
    public bool hasBullet = false;
    public bool HasBullet {
        get {
            return hasBullet;
        }

        set {
            hasBullet = value;

        }
    }

    private bool fire = false;
    public bool Fire {
        get {
            return fire;
        }

        set {
            fire = value;

        }
    }
    // Update is called once per frame
    void Update()
    {
        if(!hasBullet) {
            GameObject bullet = Instantiate(bulletPrefab, transform);
            // bullet.SetActive(true);
            hasBullet = !hasBullet;
        } else {
            if (fire) {
                GameObject bullet = transform.GetChild(0).gameObject;
                bullet.transform.forward = new Vector3(1, 0, 0);
                Rigidbody rb = bullet.GetComponent<Rigidbody>();
                rb.velocity = transform.forward.normalized * bulletSpeed;
                bullet.transform.parent = null;
                Destroy(bullet, 10);
                fire = !fire;
            }
        }
    }
    
}
